<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatesDgroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dgroups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dgroup_name');
            $table->integer('dgroup_leader_id');
            $table->longText('schedule');
            $table->enum('status',['open', 'closed']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dgroups');
    }
}
