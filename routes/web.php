<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//check if db connected 
/*$pdo = DB::connection()->getPdo();

if($pdo)
{
 echo "Connected successfully to database ".DB::connection()->getDatabaseName();
} else {
 echo "You are not connected to database";
}*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/hello', function(){
	return view('hello');
});


/*Route::get('/events', function(){
	$variable = array('test','test2');
	return view('events.index',['data' => $variable]);
});*/
Route::get('/events', 'EventController@index');
Route::get('/events/new', 'EventController@new');
Route::post('/events/add', 'EventController@add');
Route::get('/event/{id}', 'EventController@view');

Route::get('/event/register/{event_id}', 'EventController@register');
Route::get('/event/register/{event_id}', 'EventController@register');

Route::post('/event/member/register','MemberController@add');
Route::post('/members/search/', 'MemberController@search');