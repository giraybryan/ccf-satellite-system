@extends('layouts.app')

@section('title','Event')
@section('content')
	<div class="container">
		<h1>{{ $event->event_name }}</h1>
		<p style="font-size: 11px;">{{ date('M d, Y  g:s a', strtotime($event->event_date))}}</p>

		<br/>
		<h4>Attendees</h4>
			<div class="row">
    			<div class="col-sm">
    				<form id="ajaxsearch" action="{{ url('members/search') }}" method="post">
    					@csrf
				    	<div class="input-group mb-3">
						  <input type="text" name="namesearch" class="form-control" placeholder="Name Search" aria-label="Name search" aria-describedby="basic-addon2">
						  <div class="input-group-append">
						    <button type="submit" class="input-group-text" id="basic-addon2">Search</button>
						  </div>
						</div>
						<div class="search_result hide" style="position: absolute; top: 37px;  padding: 10px;    background: #fff;    border: 1px solid #ddd;    width: 94%;">sdf</div>
					</form>
    			</div>
    			<div class="col-sm">
    				<a class='btn btn-primary' href='{{ url("event/register/".$event->id) }}'>register</a>
    			</div>
    		</div>
		
		<table class="table">
			<thead>
				<tr>
					<th>Name</th>
					<th>Number</th>
					<th>Age</th>
					<th>Dgroup</th>
					<th>BOG Assign</th>
				</tr>
			</thead>
			@forelse($attendees as $attendee)
				<tr>
					<td>{{ $attendee->firstname.' '.$attendee->lastname }}</td>
					<td>{{ $attendee->number}}</td>
					<td>{{ date('Y') - date('Y', strtotime($attendee->bod) ) }}</td>
					<td>{{ $attendee->dgroup == 1 ? 'Yes' : 'No' }}</td>
					<td>
						<a class='btn btn-info' href='{{ url("event/".$event->id)}}'>View</a>
						<a class='btn btn-default' href='{{ url("event/edit/".$event->id)}}'>Edit</a>
						<a class='btn btn-danger' href='{{ url("event/erase/".$event->id)}}'>Erase</a>
					</td>
				</tr>
				
			@empty
				<tr>
					<td colspan="5"><div class='alert alert-danger'>Empty</div></td>
				</tr>
			@endforelse
		</table>
	</div>
@endsection

@section('javascript')
<script src="{{ asset('js/modules/membersearch.js') }}" defer></script>
@endsection