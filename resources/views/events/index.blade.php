@extends('layouts.app')

@section('title','Event')
@section('content')

<div class='container'>
	
	<a href="{{ url("/events/new") }}" class="btn btn-primary" style="margin-bottom: 10px;">Add Event</a>
	<table class="table">
		<thead>
			<tr>
				<th>Event Name</th>
				<th>Event Date</th>
				<th>Action</th>
			</tr>
		</thead>
	@forelse($events as $event)
		<tr>
			<td>{{ $event->event_name }}</td>
			<td>{{ date('M d, Y g:i a', strtotime($event->event_date) ) }}</td>
			<td>
				<a class='btn btn-info' href='{{ url("event/".$event->id)}}'>View</a>
				<a class='btn btn-default' href='{{ url("event/edit/".$event->id)}}'>Edit</a>
				<a class='btn btn-danger' href='{{ url("event/erase/".$event->id)}}'>Erase</a>
			</td>
		</tr>
		
	@empty
		<tr>
			<td colspan="3"><div class='alert alert-danger'>No Events</div></td>
		</tr>
	@endforelse
	</table>
</div>
@endsection