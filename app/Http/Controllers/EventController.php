<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    //
    public function index()
    {
        $date = date('Y-m-d');
    	$events = DB::table('events')
                    ->where('event_date','>=',$date)
                    ->get();

    	return view('events.index',['events' => $events]);
    }

    public function register($event_id = 0){
    	//get menus
    	$menus = \App\Menu::all();
    	return view('events.register', ['menus' => $menus, 'event_id' => $event_id ]);
    }

    public function new()
    {   
        $ministries = \App\Ministry::all();
        return view('events.new', ['ministries' => $ministries]);
    }

    public function add()
    {
        $event =  new \App\Event();
        $data = request()->validate([
            'event_name'    => 'required|max:200',
            'event_date'    => 'required',
            'time'          => 'required',
            'ministry_id'   => 'required',
        ]);
        

        $event->event_date = date('Y-m-d',strtotime(request('event_date'))).' '.request('time').':00';
        $event->event_name = request('event_name');
        $event->ministry_id = request('ministry_id');
        $event->save();

        return redirect('/events');


    }

    public function view($id = 0)
    {
        $event = DB::table('events')
                    ->where('id',$id)
                    ->first();

        $attendees = DB::table('attendees')
                        ->join('members','members.id','=','attendees.member_id')
                        ->select('attendees.*','members.firstname', 'members.lastname')
                        ->where('attendees.event_id','=', $id)
                        ->get();

        return view('events.view',['event' => $event,'attendees' => $attendees]);
                
    }
}
