<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MemberController extends Controller
{
    //
    public function add()
    {
    	//print_r($_POST);	
    	$member = new \App\Member();

    	$data = request()->validate([
    		'firstname' => 'required|max:100',
    		'lastname'	=> 'required|max:100',
    		'email'		=> 'required|unique:members|email',
    		'bod'		=> 'required|date',
    		'number'	=> 'required|numeric',
    	]);

    	$member->bod = date('Y-m-d',strtotime(request('bod')));
    	$member->created_at = date('Y-m-d');
    	$member->updated_at = date('Y-m-d');
    	$member->firstname = request('firstname');
    	$member->lastname = request('lastname');
    	$member->dgroup = request('dgroup');
    	$member->number = request('number');
    	$member->email = request('email');
    	$member->fb = request('fb');
    	$member->save();

    	return redirect()->back();
    }

    public function search()
    {
    	$search = request('namesearch');
    	$search_member = DB::table('members')
                    ->where('firstname','=',$search)
                    ->orWhere('lastname', $search)
                    ->get();

        return response()->json($search_member);

    }
}
